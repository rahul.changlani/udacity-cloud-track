# Create VPC

![Create VPC 1](create_VPC1.png)
![Create VPC 2](create_VPC2.png)
![Create VPC 3](create_VPC3.png)

# Create VPC (Alternate way)

![Create VPC Alternate 1](create_VPC_alt1.png)
![Create VPC Alternate 2](create_VPC_alt2.png)
