# Elastic Beanstalk

## Create

![Create Elastic Beanstalk 1](./01_create/elastic_beanstalk_create_1.png)
![Create Elastic Beanstalk 2](./01_create/elastic_beanstalk_create_2.png)
![Create Elastic Beanstalk 3](./01_create/elastic_beanstalk_create_3.png)
![Create Elastic Beanstalk 4](./01_create/elastic_beanstalk_create_4.png)
![Create Elastic Beanstalk 5](./01_create/elastic_beanstalk_create_5.png)
![Create Elastic Beanstalk 6](./01_create/elastic_beanstalk_create_6.png)
![Create Elastic Beanstalk 7](./01_create/elastic_beanstalk_create_7.png)

## Test

![Test Elastic Beanstalk 1](./02_test/elastic_beanstalk_test_1.png)

## Cleanup

![Cleanup Elastic Beanstalk 1](./03_cleanup/elastic_beanstalk_cleanup_1.png)
![Cleanup Elastic Beanstalk 2](./03_cleanup/elastic_beanstalk_cleanup_2.png)
![Cleanup Elastic Beanstalk 3](./03_cleanup/elastic_beanstalk_cleanup_3.png)
