# BASH & COMMAND LINE


## What is a shell?

> Shell is the outermost part of an operating system.It is designed to provide a way for us to interact with the tools of our operating system.
> E.g. One of the most widely used shell is BASH.


## What is a termial?

> Terminal is an interface to communicate with the shell.It is a program that runs the shell.
> E.g. Browser used to browse websites, likewise the terminal is used to run shell. 


## SHELL COMMANDS

- Echo:Same as print in python.

- !!:Used to denote previous command in shell.

- THE DOLLAR($) SYMBOL 
> The dollar symbol in front of a word indicates it is a shell variable.

---------------------------------------------------------

### KEYWORDS
a. COLUMNS  (case-sensitive)
b. LINES  (CASE SENSITIVE)

---------------------------------------------------------

- PWD(PRINT WORKING DIRECTORY)
> It will print the path of our current working directory.

- PATH
> Several directory names seperated by forward slashes is called PATH.

- cd(CHANGE DIRECTORY)
> Changes the path.

- ls(LIST)
> Lists the subdirectories in the current directory.
>
> - .. (Parent directory)
> Denotes the parent directory.
> E.g.
> cd .. (Changes the directory to parent directory)
> ls .. (Lists the sub-directories in the parent directory)
> NOTE: Parent Directory is the directory just before the current directory or the directory in which it is.
>
> - .(Current Directory)
> (ls .) is the same as (ls)
>
> - ~(TILDE)
> Denotes HOME DIRECTORY(The main directory which contains all other directories)

### ABOLUTE vs RELATIVE PATH
1. RELATIVE PATH
(Gives the path relative to your current location) 
2. ABSOLUTE PATH
(The other option is to give an absolute path. This is where you provide the full path, all the way from the home directory.)


```Bash
ls -l
```
> (-l) is long and is used to get detailed/long information of the subdirectories.
> E.g.
> (ls -l Rajas) will show the subdirectories in the Rajas folder with long/detailed information.


```Bash
ls -l *.pdf
```
> E.g.
> 1. (ls -l Rajas/*.pdf) will show the subdirectories of Rajas with .pdf extension in long/detailed information.
> 2. (ls -l *.pdf) will show the subdirectories of current direcrtory with .pdf extension in long/detailed extension.


```Bash
ls *er*
```
> It will search the directory name for 'er' string
> E.g. LearnerLicense.pdf


```Bash
ls er*
```
> It will search/match the name from the start.
> E.g. erwinbook.pdf 
> NOTE: But will not match LearnerLicense.pdf


### MAKE FILE

```Bash
echo 'Hello' > Myfile.txt
```

### MKDIR (Make Directory)

```Bash
mkdir <filename>
```
> mkdir File (It will create a folder named 'File' in the current directory)
> mkdir Rajas/File (It will create a folder named 'File' in the subdirectory(Rajas) of the current directory.

### MV(Move directory)

```Bash
mv 'File name/location' Destination
```
> E.g.
> If i am on desktop of this pc
> mv 'Rajas/AI.txt' 'Rajas/Saved Pages' 

----------------------------------------

### CURL(C URL or See URL)
> curl 'www.google.com' ---- The curl command can be used to download a file from the Web. By default, it will download the file and display the contents in the terminal

> - -L (Following Redirects) {Capital L}
> E.g. When you search ww.googel.com you are automatically redirected to the www.google.com site.
> ``` curl -L www.googel.com ``` is same as ```curl -L www.google.com```
>
> - (-o) OUTPUT TO A FILE{Small o}
> E.g. ``` curl 'www.google.com' -o somefile.html ```
> This will automatically save the code of google  site to somefile.html but with missing images.

### CAT (Concatenate)
> The cat command will display the full contents of the file.

### LESS
> This displays the output in a format that allows you to search and scroll.

### RMDIR (Removes Directory)
> Can also delete multiple directories.
> E.g.
> ``` rmdir file1 file2 ```

### RM(Remove File)
> Can also delete multiple files like directories.
> - -i(Interactive)
> E.g. ``` rmdir -i file1 ```
> Will ask before removing file to make sure the user dosen't delete it by mistake.
