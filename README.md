# Udacity cloud track
This is a list of notes for Udacity's first phrase of cloud track scholarship challenge

## AWS
* [Create VPC](./AWS/01_create_vpc)
* [Create EC2](./AWS/02_create_ec2_with_storage)
* [Cleanup VPC & EC2](./AWS/03_cleanup)
* [Create Lamdba](./AWS/04_lambda)
* [Create elastic beanstalk](./AWS/05_elastic_beanstalk)
* [Create DynamoDb](./AWS/06_dynamo_db)
* [Create RDS](./AWS/07_rds)
* [Create Cloudfront](./AWS/08_cloud_front)
* [Create IAM](./AWS/09_iam)
* [Create Autoscaling](./AWS/10_autoscaling)
* [SNS](./AWS/11_sns)
* [Cloudwatch](./AWS/12_cloudwatch)


## Git



## Shell