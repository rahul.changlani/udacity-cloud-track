# GIT

## GIT TERMINOLOGIES
1. VERSION CONTROL SYSTEM/SOURCE CODE MANAGER
>  Version Control System is a tool that manages diffferent versions of surce code.Source code manager is the same    name for Version control system.

2. COMMIT
>  Commit is to save that particular state of project.

3. REPOSITORY/REPO
>  Repository is a directory which contains all commits and hiddenfiles(used by git)

4. WORKING DIRECTORY
>  The Working directory are the files that are stored inside the computer's file system.
  This is in contrast to the files that are saved in the repository.

5. CHECKOUT
>  A checkout is when the content in the repository is copied to the working directory.

6. STAGING INDEX
>  Files in the GIT DIRECTORY stores information about what will go to our next commit.
>  STAGING AREA is the area from where the git will take its next commmit.
>  NOTE: Files need to be first sent to the staging area for making commit in repository.

7. SHA (Secure Hash Algorithm)
>  SHA is basically an ID for every commit.
>  It is 40 character string cosisting of (a-f) and (0-9).

8. BRANCH
>  Branch is a new line of development that diverges form the main line of development.
>  Changes in the alternative line of development can continue without altering the main line.


### COMMANDS

1. CREATE A GIT REPOSITORY

> ```Bash
> git init
> ``` 
> Running the git init command sets up all of the necessary files and directories that Git will use to keep track of everything. All of these files are stored in a directory called .git
> NOTE: .git is the repository which contains all the necessary files used by git.
>
> #### FILES INSIDE THE .GIT DIRECTORY
>
> - config file: where all project specific configurations are stored like username, email etc.
> - description file: used by gitweb program.
> - hooks : contains client side and server side scripts that we can use to hook into git's lifecycle events.
> - info : contains global excludes file(dont know meaning)
> - objects : will store all the commmits we make.
> - refs: will store pointers to commits such as tags, branches etc.
>
> NOTE: TRACKED FILES AND UNTRACKED FILES
>
> Untracked files are the ones that are in your working directory.Git dosen't know about them.
> Tracked files are the files about which Git knows.


2. CLONE AN EXISTING GIT REPOSITORY
> ```Bash
> git clone https://github.com/udacity/course-git-blog-project
> ```
> 
> This will create a repository in your working directory(cd to your WD, repo can not be made inside a repo)
> by default with the name course-git-blog-project.
> 
> To change the name of the repository in your WD to PROJECT_1
> ```Bash
> git clone https://github.com/udacity/course-git-blog-project PROJECT_1
> ```


3. REVIEW A REPOSITORY'S HISTORY
> ```Bash
> git status
> ```
> > Git status tells the current status of the repository.
> 
> ```Bash
> git log
> ```
> > git log displays:
> > - SHA 
> > - AUTHOR
> > - DATE TIME
> > - MESSAGE
> >
> > By default the git log navigates in less(which displays the command line in the form of pager)
> > SOME USEFUL KEYS TO SCROLL ARE:
> > j:to move down one line at a time.
> > d: to move down half a page at a time.
> > f: to move down full page at a time.
> > k: to move up one line at a time.
> > u: to move up half a page at a time.
> > b: to move up full page at a time.
> > q: quit
> > 
> > #### FLAG (Flags can be used to alter how a program functions and/or what is displayed)
> - ```git log --oneline```
> > It will work same as git log, but displays in a more compact way.
> > '7letters-of-SHA' and 'commit'.
> > e.g.1ae234e CHANGE IN HEADER TAG
> >
> - ```git log --stat```
> > It works the same as git log , but adds some more information:
> > - Name of the files which were changed
> > - No. of lines added or deleted per file
> > - (At the bottom)Total no. of lines added in the commit. & Total no. of lines deleted in the commmit.
> >
> > #### WHITESPACES
> > Whitespace is any character or series of characters that represent horizontal or vertical space in typography  
> >
> > #### INDICES
> > 1 indice is 4 spaces and 2 indices is 8 spaces.
> > 
> - ```git log -p``` or ```git log --patch```
> > The git log command has a flag that can be used to display the actual changes made to a file. The flag is --patch which can be shortened to just -p:
> > e.g. The lines which were removed and the lines which were added.

> - ```git log -p -w``` or ```git log --patch -w```
> > This ingores the whitespaces.


-------------------------------

### ADDING COMMITS TO A REPOSITORY
```Bash
git add 
```
> Add files from the working directory to the staging index.

```Bash
git commit
```
> Takes files from the working directory to the repository.

```Bash
git diff
```
> Displays the difference between the two versions of the file.

```Bash
git rm -cached "file name"
```
> To remove the file from the staging index if added accidently.

```Bash
git add .
```
> To add all the files in the current directory and its subdirectories.

```Bash
git commit
```
> To add commit in the repository

```Bash
git commit -m "commit description"
```
> -m is used to add commit description in the CLI itself(instead of opening the code editor)

```Bash
git diff
```
> The git diff command is used to see changes that have been made but haven't been committed

```Bash
touch
```
> touch command can be used to create new empty files.
> E.g.
> ```Bash
> touch sad.txt
> ```

--------------------------------
--------------------------------



### gitignore
If we want some files to get ignored in the working directory.
Then create .gitignore file using 
touch .gitignore
(It is by default a text document)

Now write inside the text document names of the files that we want to be ignored using

blank lines can be used for spacing
# - marks line as a comment
* - matches 0 or more characters
? - matches 1 character
[abc] - matches a, b, _or_ c
** - matches nested directories - a/**/z matches
a/z
a/b/z
a/b/c/z

--------------------------------TAGGING , BRANCHING AND MERGING----------------------------------------------------------------

git tag v1.0
---This command tags the most recent commit with the name "v1.0"

git tag -a v1.0
---This command tags the most recent commit and also opens a code editor for writing more information about the tag.

-a is the flag which denotes "annotated"

git tag
Displays the names of the tags present in the repository.

git log --decorate or git log or git log --tags

Displays the output of git log and in addition the names of tag ahead the repository.

DELETING A TAG

git tag -d v1.0 or git tag --delete v1.0
Deletes the tag named "v1.0"

ADDING A TAG TO A PAST COMMIT

git tag -a v1.0 5aae45 (This will tag the commit with the SHA provided)

GIT BRANCH CONCEPT(UDACITY ->LESSON 7 ->BRANCHING ->VIDEO)

git branch
This shows all the existing branches in the repository.

-----------NOTE: By default when a repository is made the current branch is named master.
                 The master branch is pointed by a pointer named "HEADER".
                 By default the master branch points the most recent commit, and the header points the master branch. 
                         
git branch sidebar
This will create a branch named "sidebar".

------------------NOTE:Whenever a new branch is created the branch will point at the commit to which the header is pointing.

git checkout sidebar
This command will make the header point to the sidebar branch.

-----------------NOTE:	The branch which the header is pointing can be changed using the checkout command.
                         Whenever a git checkout command is run the system:
                         1. will delete all working directory files from its tracking list
                         2. go into the repository and pull all the files of the commit that the branch point to.


DELETE A BRANCH
git branch -d sidebar

Forceful Delete of branch 
git branch -D sidebar


NOTE: Whenever we switch on another branch only the contents of that branch is visible in the working directory.
      
SWITCH AND CREATE BRANCH IN ONE COMMAND
git checkout -b bar (This will create a branch pointing to the commit to which the header is pointing, and will checkout in the 'bar' branch.)

git checkout -b bar master(This will create the branch 'bar' which will point to the commit same as the master branch, and will checkout in the 'bar' branch)

MERGE
Combining branches together is called Merge.

2 Types of Merge:
1. Regular Merge(Because this combines two divergent branches, a commit is going to be made, the new merge commit is pointed by the branch which header points. )
2. Fast Forward Merge(When the branch to be merged is directly ahead of the header branch,
                      A Fast-forward merge will just move the currently checked out branch forward until it points to the same commit that the other branch)

To undo a Merge
git reset --hard HEAD^ 

MERGE CONFLICT
A merge conflict happens when the same line or lines have been changed on different branches that are being merged. Git will pause mid-merge telling you that there is a conflict and will tell you in what file or files the conflict occurred. To resolve the conflict in a file:



So , in the conflicted file the git denotes the the changes by MERGE CONFLICT INDICATORS.
<<<<<<< HEAD everything below this line (until the next indicator) shows you what's on the current branch
||||||| merged common ancestors everything below this line (until the next indicator) shows you what the original lines were
======= is the end of the original lines, everything that follows (until the next indicator) is what's on the branch that's being merged in
>>>>>>> heading-update is the ending indicator of what's on the branch that's being merged in (in this case, the heading-update branch)

After the necessary changes run the commmand 'git commit', this command  will resume the merging.

------------------------------------------------UNDOING AND CHANGES----------------------------------------------
                       
git commit --amend - amending the previous commmit.

When the working directory is clean , running the git commit --amend command will open up the code editor with the original commit message , so we can change the commit message.

To add more files in the previous commit, 1.just add files in the working directory
                                          2.save the file 
                                          3. commit the file using git commit --amend command
     This will add the files in the previous repository.
 
git revert (SHA) - reverting the previous commmits.


git reset- moves the head and branch position to the referenced commit , and then changes like delete, shifting to working directory,shifting to staging index take place.
                       
Ancestry References
^ indicates the parent commit.	  
~ indicates the first parent commit.

(References to commit)
the Parent commit
HEAD^
HEAD~
HEAD~1

the grand parent commit
HEAD^^
HEAD~2

the great grand parent commit
HEAD^^^
HEAD~3

3 FLAGS
git reset --hard (Reference to commit) ----moves head to the reference commit, remaining commits are deleted.
git reset --soft (refernece) --------------moves head to the reference commit, remaining commits to staging index
git reset --mixed (reference) -------------moves head to the reference commit, remaining commits are moved to the                                            working directory.---------------------------



-------------------------------WORKING WITH REMOTES---------------------------------------------------------

What is a remote repository?
A remote repository is the copy of the same repository as ours but on a different machine/exists somewhere else.

COMMANDS
1.git remote
2.git push 
3.git pull

git remote
This will show all the remote repositories.

git remote add Github-link https://www.github/rajas2716/my-travel-plans.com

So this command will add a remote repository link to your current local repository, and will name it 'Github-link'

Here we can use any name apart from 'Github-link' , these names are called SHORTNAMES.

git remote -v 
This command will show all the remote repositories link along with their shortnames.
One link will ne shown two times(fetch,pull)

git push origin master

The git push command takes the shortname of the url and the branch you want to send.

After this the master branch becomes origin/master branch.

-------------NOTE:The branch that appears in the local repository , is tracking the branches in the remote repository.So , the origin/master branch is called tracking branch since it will track the master branch of the remote repository.

If there are changes in a remote repository that you'd like to include in your local repository

git pull origin master

When git pull is run, the following things happen:

the commit(s) on the remote branch are copied to the local repository
the local tracking branch (origin/master) is moved to point to the most recent commit
the local tracking branch (origin/master) is merged into the local branch (master)---------------1

NOTE: 1 is very important, whenever a git push/pull is made. A tracking branch named (shortname/master) which keeps track of the file in the remote       repository is made.
      If git pull is run ,commmits from the remote repository are copied to the tracking branch of local repository.
      Then , tracking branch(shortname/master) merges into the master branch.(Fast-Forward Merge)


git fetch origin master
In git fetch ,
commits are copied to the tracking branch from the remote repository.
the local tracking branch moves to the most recent commit.
But, they are not merged into the master branch.
